import React, { useState } from 'react'
import Input from '../Input'
import { Container, FormContent, InputContainer } from './styles'

const Form = () => {

    const [parede1, setParede1] = useState({
        p1: null,
        pt1: null,
        j1: null,
    })
    const [parede2, setParede2] = useState({
        p2: null,
        pt2: null,
        j2: null,
    })
    const [parede3, setParede3] = useState({
        p3: null,
        pt3: null,
        j3: null,
    })
    const [parede4, setParede4] = useState({
        p4: null,
        pt4: null,
        j4: null,
    })

    function handleChangeParede1({target}){
        setParede1({...parede1, [target.name]: target.value})
    }

    function handleChangeParede2({target}){
        setParede2({...parede2, [target.name]: target.value})
    }

    function handleChangeParede3({target}){
        setParede3({...parede3, [target.name]: target.value})
    }

    function handleChangeParede4({target}){
        setParede4({...parede4, [target.name]: target.value})
    }


    return (
        <Container>
            <FormContent action="">
                <h1>Calculador de Tintas</h1>
                <h2>Preencha os campos abaixo e veja quantas latas de tinta <br /> você irá precisar para pintar as paredes!</h2>

                <h3>Insira o tamanho das paredes e quantidade de janelas e portas:</h3>
                <InputContainer>
                    <Input 
                        label="Parede #1" 
                        type="number" 
                        placeholder="Exemplo: 80m²" 
                        name="p1" 
                        name2="pt1" 
                        name3="j1"
                        value={parede1.p1}
                        value2={parede1.pt1}
                        value3={parede1.j1}
                        onChange={handleChangeParede1}
                    />
                    <Input 
                        label="Parede #2" 
                        type="number" 
                        placeholder="Exemplo: 80m²" 
                        name="p2" 
                        name2="pt2" 
                        name3="j2"
                        value={parede2.p2}
                        value2={parede2.pt2}
                        value3={parede2.j2}
                        onChange={handleChangeParede2}
                    />
                    <Input 
                        label="Parede #3" 
                        type="number" 
                        placeholder="Exemplo: 80m²"
                        name="p3" 
                        name2="pt3" 
                        name3="j3"
                        value={parede3.p3}
                        value2={parede3.pt3}
                        value3={parede3.j3}
                        onChange={handleChangeParede3}
                    />
                    <Input 
                        label="Parede #4" 
                        type="number" 
                        placeholder="Exemplo: 80m²" 
                        name="p4" 
                        name2="pt4" 
                        name3="j4"
                        value={parede4.p4}
                        value2={parede4.pt4}
                        value3={parede4.j4}
                        onChange={handleChangeParede4}
                    />
                </InputContainer>
            </FormContent>
        </Container>
    )
}

export default Form
