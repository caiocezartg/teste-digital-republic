import styled from "styled-components";

export const Container = styled.div`
  max-width: 1200px;
  height: 100vh;
  margin: 0 auto;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormContent = styled.form`
  background-color: #3e4c59;
  padding: 20px 35px;
  margin: 0 30px;
  border-radius: 8px;

  h1 {
    text-align: center;
    margin-bottom: 20px;
    color: #f4901d;
    font-size: 42px;
  }
  h2 {
    text-align: center;
    font-size: 24px;
    margin-bottom: 30px;
  }
  h3 {
    text-align: center;
    margin-bottom: 20px;
    font-size: 16px;
    color: #e4e7eb;
  }
`;

export const InputContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 30px;

  background-color: #616e7c;
  padding: 20px;
  border-radius: 8px;
  }
`;
