import React from 'react'
import { InputContent } from './styles'

const Input = ({label, type, placeholder, name, name2, name3, value, value2, value3, onChange}) => {
    return (
        <InputContent>
            <label>
                {label}
                <input type={type} placeholder={placeholder} name={name} value={value} onChange={onChange} />
            </label>

            <label>
                Quantas portas possuem?
                <input type={type} placeholder='Exemplo: 1' name={name2} value={value2}  onChange={onChange}/>
            </label>

            <label>
                Quantas janelas possuem?
                <input type={type} placeholder='Exemplo: 2' name={name3} value={value3} onChange={onChange}/>
            </label>
            
        </InputContent>
    )
}

export default Input
