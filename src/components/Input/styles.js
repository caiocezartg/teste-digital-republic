import styled from "styled-components";

export const InputContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  label {
    display: flex;
    flex-direction: column;
    font-weight: 400;
    color: #cbd2d9;
  }
  input {
    margin: 5px 0 15px 0;
    padding: 10px 15px;
    outline: none;
    transition: 0.3s ease-in-out;

    :focus,
    :hover {
      border: 2px solid #f4901d;
    }
  }
`;
