import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    *{
        padding: 0;
        margin: 0;
        text-decoration: none;
        list-style-type: none;
    }
    body{
        background-color: #1F2933;
        -webkit-font-smoothing: antialiased;

        font-family: 'Arial', sans-serif;
        font-weight: 400;
        color: #F5F7FA;
    }

    h1, h2, h3, h4, h5, h6{
        font-weight: 600;
    }

    button{
        cursor: pointer;
    }
`;
